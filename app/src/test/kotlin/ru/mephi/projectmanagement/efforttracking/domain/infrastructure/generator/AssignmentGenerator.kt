package ru.mephi.projectmanagement.efforttracking.domain.infrastructure.generator

import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.domain.model.Project
import ru.mephi.projectmanagement.efforttracking.domain.model.Task
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus

object AssignmentGenerator {


    fun generateProject(
        name: String = "Project name 1",
        description: String = "Project description 1"
    ): Project {
        return Project(
            name = name,
            description = description
        )
    }

    fun generateTask(
        name: String = "Task name 1",
        project: Project = generateProject(),
        startDate: DateTime = DateTime.now().minusMonths(2),
        endDate: DateTime = DateTime.now().plusMonths(2),
        description: String = "Task description 1"
    ): Task {
        return Task(
            name = name,
            project = project,
            startDate = startDate,
            endDate = endDate,
            description = description
        )
    }

    fun generateAssignment(
        id: Int = 1,
        name: String = "Assignment title for task 1",
        description: String = "Assignment description for task 1",
        task: Task = generateTask(),
        startDate: DateTime = DateTime.now().minusDays(24),
        endDate: DateTime = DateTime.now().plusDays(24),
        estimatedHours: Float = 0f,
        assignmentStatus: AssignmentStatus = AssignmentStatus.MUTABLE
    ): Assignment {
        return Assignment(
            id = id,
            title = name,
            description = description,
            task = task,
            startDate = startDate,
            endDate = endDate,
            estimatedHours = estimatedHours,
            assignmentStatus = assignmentStatus
        )
    }

}
