package ru.mephi.projectmanagement.efforttracking.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import ru.mephi.projectmanagement.efforttracking.domain.infrastructure.generator.AssignmentGenerator

class AssignmentsTest {

    @Test
    fun `When the description is set - the description is successfully set`() {

        val assignment = AssignmentGenerator.generateAssignment(description = "")

        assertThat(assignment.description).isEmpty()

        val updatedAssignment = assignment.copy(description = "Assignment description 1")

        assertThat(updatedAssignment.description).isEqualTo("Assignment description 1")
    }

}
