package ru.mephi.projectmanagement.efforttracking.domain.interactor

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Completable
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.joda.time.DateTime
import org.joda.time.DateTimeUtils
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import ru.mephi.projectmanagement.efforttracking.domain.infrastructure.generator.AssignmentGenerator.generateAssignment
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository

class AssignmentInteractorTest {

    @Test
    fun `When the working hours is added with the correct parameters - the remaining hours is updated`() {

        val assignment = generateAssignment(
            estimatedHours = 10.5f,
            assignmentStatus = AssignmentStatus.MUTABLE
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        assignmentInteractor
            .addWorkingHours(
                assignmentId = assignment.id,
                date = DateTime.now(),
                workingHours = 3f,
                comment = "Did the job."
            )
            .blockingGet()

        assertThat(assignment.getRemainingHours()).isEqualTo(7.5f)
    }


    @Test
    fun `When the working hours is updated with the correct parameters - the remaining hours is updated`() {

        val assignment = generateAssignment(
            estimatedHours = 10.5f,
            assignmentStatus = AssignmentStatus.MUTABLE
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val date = DateTime.now()

        val transaction = assignmentInteractor
            .addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 3f,
                comment = "Did the job."
            )
            .blockingGet()

        assignmentInteractor
            .updateWorkingHours(
                assignmentId = assignment.id,
                transactionId = transaction.id,
                date = date,
                workingHours = 5f
            )
            .blockingGet()

        assertThat(assignment.getRemainingHours()).isEqualTo(5.5f)
    }

    @Test
    fun `When labor is written off with incorrect number of hours - throw Exception`() {

        val assignment = generateAssignment()

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val date = DateTime.now()

        val throwable = catchThrowable {
            assignmentInteractor.addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 28f,
                comment = "Did the job."
            ).blockingGet()
        }

        assertThat(throwable).doesNotThrowAnyException()

        val newTransaction = assignment.transactionList.first { it.date == date }

        assertThat(newTransaction.workingHours).isEqualTo(24f)
    }

    @Test
    fun `When labor is written off with incorrect date - throw Exception`() {

        val assignment = generateAssignment(
            startDate = DateTime.now().minusDays(14),
            endDate = DateTime.now().minusDays(7)
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val throwable = catchThrowable {
            assignmentInteractor
                .addWorkingHours(
                    assignmentId = assignment.id,
                    date = DateTime.now().minusDays(6),
                    workingHours = 5f,
                    comment = "Did the job."
                )
                .blockingGet()
        }

        assertThat(throwable).hasMessage("The transaction date is outside the assignment dates interval")
    }

    @Test
    fun `When transaction is changed with incorrect date - throw Exception`() {

        val assignment = generateAssignment(
            startDate = DateTime.now().minusDays(14),
            endDate = DateTime.now().minusDays(7)
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)


        val transaction = assignmentInteractor
            .addWorkingHours(
                assignmentId = assignment.id,
                date = DateTime.now().minusDays(10),
                workingHours = 5f,
                comment = "Did the job."
            )
            .blockingGet()

        val throwable = catchThrowable {
            assignmentInteractor
                .updateWorkingHours(
                    assignmentId = assignment.id,
                    transactionId = transaction.id,
                    date = DateTime.now().minusDays(6),
                    workingHours = 5f
                )
                .blockingGet()
        }

        assertThat(throwable).hasMessage("The transaction date is outside the assignment dates interval")
    }

    @Test
    fun `When labor is written off with outdated date - throw Exception`() {

        val assignment = generateAssignment(
            startDate = DateTime.now().minusDays(14),
            endDate = DateTime.now().minusDays(7)
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val date = DateTime.now().minusDays(10)

        DateTimeUtils.setCurrentMillisFixed(DateTime.now().plusDays(14).millis)

        val throwable = catchThrowable {

            assignmentInteractor
                .addWorkingHours(
                    assignmentId = assignment.id,
                    date = date,
                    workingHours = 5f,
                    comment = "Did the job."
                )
                .blockingGet()
        }


        assertThat(throwable).hasMessage("Transaction is outdated")

        DateTimeUtils.setCurrentMillisSystem()
    }


    @Test
    fun `When a transaction is changed with outdated date - throw Exception`() {

        val assignment = generateAssignment(
            startDate = DateTime.now().minusDays(14),
            endDate = DateTime.now().minusDays(7)
        )

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)


        val date = DateTime.now().minusDays(10)

        val transaction = assignmentInteractor
            .addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 5f,
                comment = "Did the job."
            )
            .blockingGet()

        DateTimeUtils.setCurrentMillisFixed(DateTime.now().plusDays(14).millis)

        val throwable = catchThrowable {
            assignmentInteractor
                .updateWorkingHours(
                    assignmentId = assignment.id,
                    transactionId = transaction.id,
                    date = date,
                    workingHours = 5f
                )
                .blockingGet()
        }

        assertThat(throwable).hasMessage("Transaction is outdated")

        DateTimeUtils.setCurrentMillisSystem()

    }

    @Test
    fun `When labor is written off with big number of hours second time - throw Exception`() {
        val assignment = generateAssignment()

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val date = DateTime.now().minusDays(1)

        val firstTime = catchThrowable {
            assignmentInteractor.addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 20f,
                comment = "Did the job."
            ).blockingGet()
        }

        assertThat(firstTime).doesNotThrowAnyException()

        val secondTime = catchThrowable {

            assignmentInteractor.addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 28f,
                comment = "Did the job."
            ).blockingGet()
        }

        assertThat(secondTime).hasMessage("The number of working hours per day more than 24 hours")
    }

    @Test
    fun `When a transaction is changed that does not exist - throw Exception`() {

        val assignment = generateAssignment()

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val throwable = catchThrowable {
            assignmentInteractor
                .updateWorkingHours(
                    assignmentId = assignment.id,
                    transactionId = -1,
                    date = DateTime.now().minusDays(4),
                    workingHours = 6f
                )
                .blockingGet()
        }

        assertThat(throwable).hasMessage("Transaction does not exist")
    }

    @Test
    fun `When immutable transactions is changed - throw Exception`() {

        val assignment = generateAssignment(assignmentStatus = AssignmentStatus.IMMUTABLE)

        val mockAssignmentRepository = mock<AssignmentRepository> {
            on { getAssignment(anyInt()) } doReturn Single.just(assignment)
            on { setAssignment(any()) } doReturn Completable.complete()
        }

        val assignmentInteractor = AssignmentInteractor(mockAssignmentRepository)

        val date = DateTime.now()

        val transaction = assignmentInteractor
            .addWorkingHours(
                assignmentId = assignment.id,
                date = date,
                workingHours = 3f,
                comment = "Did the job."
            )
            .blockingGet()

        val throwable = catchThrowable {
            assignmentInteractor
                .updateWorkingHours(
                    assignmentId = assignment.id,
                    transactionId = transaction.id,
                    date = date,
                    workingHours = 6f
                )
                .blockingGet()
        }

        assertThat(throwable).hasMessage("Transaction is immutable")
    }
}
