package ru.mephi.projectmanagement.efforttracking.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.joda.time.DateTime
import org.junit.Test
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkRate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkType

class TransactionTest {

    @Test
    fun `When the work type is set - the work type is successfully set`() {
        val start = DateTime.now().minusDays(2)

        val transaction = Transaction(
            id = 1,
            date = start,
            workingHours = 5f,
            comment = "",
            workType = WorkType.STANDARD,
            workRate = null
        )

        assertThat(transaction.workType).isEqualTo(WorkType.STANDARD)

        val updatedTransaction = transaction.copy(workType = WorkType.OVERTIME)
        assertThat(updatedTransaction.workType).isEqualTo(WorkType.OVERTIME)
    }


    @Test
    fun `When the work rate is set - the work rate is successfully set`() {
        val start = DateTime.now().minusDays(10)

        val transaction = Transaction(
            id = 1,
            date = start,
            workingHours = 5f,
            comment = "",
            workType = null,
            workRate = WorkRate.X_1
        )

        assertThat(transaction.workRate).isEqualTo(WorkRate.X_1)

        val updatedTransaction = transaction.copy(workRate = WorkRate.X_2)
        assertThat(updatedTransaction.workRate).isEqualTo(WorkRate.X_2)
    }
}
