package ru.mephi.projectmanagement.efforttracking.domain.model.assignment

enum class WorkRate(val value: String) {
    X_1("Одинарный"),
    X0_5("Половинчатый"),
    X_2("Двойной")
}
