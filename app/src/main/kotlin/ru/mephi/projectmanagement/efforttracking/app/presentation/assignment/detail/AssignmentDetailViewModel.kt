package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.detail

import androidx.lifecycle.MutableLiveData
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import ru.mephi.projectmanagement.efforttracking.app.extension.onNext
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.SchedulersProvider
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.scheduleIoToUi
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import timber.log.Timber

class AssignmentDetailViewModel @AssistedInject constructor(
    @Assisted private val assignmentId: Int,
    private val assignmentRepository: AssignmentRepository,
    private val schedulers: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val TAG = "AssignmentDetailVM"
    }

    val state = MutableLiveData<Assignment>()

    init {
        loadAssignment()
    }

    private fun loadAssignment() {
        assignmentRepository
            .getAssignment(assignmentId)
            .scheduleIoToUi(schedulers)
            .safeSubscribe(
                onSuccess = { state.onNext(it) },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    commands.onNext(ShowSnackbarError(message))
                }
            )
    }

    @AssistedInject.Factory
    interface Factory {
        fun create(assignmentId: Int): AssignmentDetailViewModel
    }

}
