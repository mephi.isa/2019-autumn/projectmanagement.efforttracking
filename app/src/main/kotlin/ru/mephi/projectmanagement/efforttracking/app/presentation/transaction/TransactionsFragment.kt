package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.fragment_transactions.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.extension.isNetworkException
import ru.mephi.projectmanagement.efforttracking.app.extension.observe
import ru.mephi.projectmanagement.efforttracking.app.extension.removeAdapterOnViewDetachedFromWindow
import ru.mephi.projectmanagement.efforttracking.app.extension.viewModels
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarMessage
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ViewCommand
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.SimpleItem
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.sticky_header.GroupieStickyHeaderAdapter
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.sticky_header.StickyHeaderDecoration
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate.LoadingWithErrorStateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.TransactionsViewState.*
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction
import javax.inject.Inject

class TransactionsFragment : Fragment(R.layout.fragment_transactions) {

    val args: TransactionsFragmentArgs by navArgs()

    private val topAnchorView = SimpleItem(R.layout.view_empty)
    private val transactionSection = Section()

    @Inject
    lateinit var viewModelFactory: TransactionsViewModel.Factory

    private val viewModel by viewModels { viewModelFactory.create(args.assignmentId) }

    private lateinit var screenState: LoadingWithErrorStateDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as App).appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(viewModel.state, this::handleState)
        observe(viewModel.commands, ::handleCommand)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        fragment_transactions_recycler.removeAdapterOnViewDetachedFromWindow()
        super.onDestroyView()
    }

    private fun initViews() {
        screenState = LoadingWithErrorStateDelegate(
            contentView = fragment_transactions_recycler,
            loadingView = fragment_transactions_container_progress,
            errorView = fragment_transactions_error_view
        )

        fragment_transactions_toolbar.setNavigationOnClickListener {
            requireView().findNavController().navigateUp()
        }

        with(fragment_transactions_recycler) {
            layoutManager = LinearLayoutManager(context)
            adapter = GroupieStickyHeaderAdapter().apply {
                add(topAnchorView)
                add(transactionSection)
            }

            addItemDecoration(
                StickyHeaderDecoration(
                    this,
                    adapter as StickyHeaderDecoration.StickyHeaderInterface
                )
            )
        }

        fragment_transactions_button_add.setOnClickListener {
            val action = TransactionsFragmentDirections.onAddTransactionClick(assignmentId = args.assignmentId)
            requireView().findNavController().navigate(action)
        }
    }

    private fun showLoading() {
        screenState.showLoading()
    }

    private fun handleState(viewState: TransactionsViewState) {
        when (viewState) {
            Loading -> showLoading()
            is Content -> {
                if (viewState.transactions.isEmpty()) {
                    handleStub()
                } else {
                    handleContent(viewState.transactions, viewState.assignmentStatus)
                }
            }
            is Error -> handleError(viewState.error)
        }

    }

    private fun handleContent(
        transactions: List<Transaction>,
        assignmentStatus: AssignmentStatus
    ) {
        var lastDate = -1

        val items = mutableListOf<Item>()

        transactions.forEach { transaction ->
            val currentDay = transaction.date.dayOfYear

            if (currentDay != lastDate) {
                items.add(TransactionHeaderItem(transaction.date))
                lastDate = currentDay
            }

            items.add(
                TransactionItem(
                    transaction = transaction,
                    assignmentStatus = assignmentStatus,
                    onEditClick = ::onEditTransactionClick
                )
            )
        }

        fragment_transactions_recycler.post {
            transactionSection.update(items)
        }

        screenState.showContent()
    }

    private fun handleStub() {
        fragment_transactions_error_view.showStub()
        screenState.showError()
    }

    private fun handleError(error: Throwable) {
        fragment_transactions_error_view.showError(error.isNetworkException())
        screenState.showError()
    }

    private fun handleCommand(command: ViewCommand) {
        when (command) {
            is ShowSnackbarMessage -> (requireActivity() as BaseActivity).showMessage(messageText = command.message)
            is ShowSnackbarError -> (requireActivity() as BaseActivity).showError(messageText = command.message)
        }
    }

    private fun onEditTransactionClick(transaction: Transaction) {
        val action = TransactionsFragmentDirections.onEditTransactionClick(
            assignmentId = args.assignmentId,
            transactionId = transaction.id
        )
        requireView().findNavController().navigate(action)
    }

}
