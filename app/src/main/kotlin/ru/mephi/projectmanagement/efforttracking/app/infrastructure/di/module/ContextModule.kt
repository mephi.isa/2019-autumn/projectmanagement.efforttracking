package ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private val context: Context) {

    @Provides
    fun context(): Context {
        return context.applicationContext
    }
}
