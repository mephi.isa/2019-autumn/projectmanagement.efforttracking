package ru.mephi.projectmanagement.efforttracking.data.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import ru.mephi.projectmanagement.efforttracking.data.network.adapters.DateTimeAdapter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoshiFactory @Inject constructor() {

    val networkMoshi: Moshi by lazy {
        getCommonMoshiBuilder()
            .build()
    }

    val storageMoshi: Moshi by lazy {
        getCommonMoshiBuilder()
            .build()
    }

    private fun getCommonMoshiBuilder(): Moshi.Builder {
        return Moshi.Builder()
            .add(DateTimeAdapter())
    }
}

inline fun <reified T> Moshi.objectToJson(obj: T): String? {
    val adapter = adapter<T>(T::class.java)
    return adapter.toJson(obj)
}

inline fun <reified T> Moshi.jsonToObject(json: String): T? {
    val adapter = adapter<T>(T::class.java)
    return adapter.fromJson(json)
}

inline fun <reified T> Moshi.listToJson(list: List<T>): String? {
    val adapter = adapter<List<T>>(Types.newParameterizedType(List::class.java, T::class.java))
    return adapter.toJson(list)
}

inline fun <reified T> Moshi.jsonToList(json: String): List<T>? {
    val adapter = adapter<List<T>>(Types.newParameterizedType(List::class.java, T::class.java))
    return adapter.fromJson(json)
}
