package ru.mephi.projectmanagement.efforttracking.domain.model.assignment

enum class AssignmentStatus(val value: String) {
    MUTABLE("изменяемое"),
    IMMUTABLE("неизменяемое")
}
