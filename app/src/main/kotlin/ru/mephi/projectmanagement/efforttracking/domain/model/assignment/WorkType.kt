package ru.mephi.projectmanagement.efforttracking.domain.model.assignment

enum class WorkType(val value: String) {
    STANDARD("Стандартный"),
    OVERTIME("Переработка"),
    STUDYING("Обучение"),
    OTHER("Другое")
}
