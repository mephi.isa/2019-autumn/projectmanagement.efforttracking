package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkRate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkType

sealed class EditTransactionViewState {

    object Loading : EditTransactionViewState()

    data class Content(
        val time: String = "",
        val date: DateTime? = null,
        val workRate: WorkRate = WorkRate.X_1,
        val workType: WorkType = WorkType.STANDARD,
        val comment: String = "",
        val saveButtonEnabled: Boolean = false
    ) : EditTransactionViewState()
}
