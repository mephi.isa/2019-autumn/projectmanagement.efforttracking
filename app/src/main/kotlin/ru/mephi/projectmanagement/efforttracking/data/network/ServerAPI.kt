package ru.mephi.projectmanagement.efforttracking.data.network

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import ru.mephi.projectmanagement.efforttracking.data.network.request.auth.LoginRequest
import ru.mephi.projectmanagement.efforttracking.domain.model.User
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment

interface ServerAPI {

    @POST("auth")
    fun login(
        @Body loginRequest: LoginRequest
    ): Single<User>

    @GET("usertasks/{user_id}")
    fun getTasks(@Path("user_id") userId: Int): Single<List<Assignment>>

}
