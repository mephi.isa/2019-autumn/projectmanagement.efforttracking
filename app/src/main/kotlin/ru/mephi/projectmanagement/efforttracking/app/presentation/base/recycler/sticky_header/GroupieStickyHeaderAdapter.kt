@file:Suppress("PackageName")

package ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.sticky_header

import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder

/**
 * Адаптер для реализации sticky-header
 * Это заголовки, прилипающие к верху списка при его листании
 * Используется, например, для того, чтобы прикрепить дату при листании списка транзакций
 *
 * Использование:
 *  // Во фрагменте
 *  with(recycler) {
 *       // Создаем GroupieStickyHeaderAdapter
 *       val regionsAdapter = GroupieStickyHeaderAdapter()
 *
 *       // Устанавливаем адаптер в RecyclerView
 *       adapter = regionsAdapter
 *       layoutManager = LinearLayoutManager(requireContext())
 *
 *       // Добавляем декоратор StickyHeaderDecoration
 *       addItemDecoration(StickyHeaderDecoration(this, adapter as StickyHeaderDecoration.StickyHeaderInterface))
 *  }
 *
 *   // Реализуем Item, который будет прилипать к верху списка
 *   data class RegionHeaderItem(val letter: Char) : Item(), StickyHeaderItem {
 *
 *       override fun getId(): Long = letter.toLong()
 *
 *       override fun getLayout(): Int = R.layout.item_region_header
 *
 *       override fun bind(viewHolder: ViewHolder, position: Int) {
 *           bindToViewHolder(viewHolder)
 *       }
 *
 *       // Метод, который заполняет вью данными.
 *       // Используется и в bind и в адаптере для напонения прилипшего заголовка данными
 *       override fun bindToViewHolder(viewHolder: ViewHolder) {
 *           with(viewHolder) {
 *               item_region_header_text_view.text = letter.toString()
 *           }
 *       }
 *   }
 */
class GroupieStickyHeaderAdapter : GroupAdapter<GroupieViewHolder>(),
    StickyHeaderDecoration.StickyHeaderInterface {

    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var position = itemPosition
        var headerPosition = 0

        do {
            if (isHeader(position)) {
                headerPosition = position
                break
            }
            position -= 1
        } while (position >= 0)

        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int) = getItem(headerPosition).layout

    override fun bindHeaderData(headerViewHolder: GroupieViewHolder, headerPosition: Int) {
        (getItem(headerPosition) as? StickyHeaderItem)?.bindToViewHolder(headerViewHolder)
    }

    override fun isHeader(itemPosition: Int) = getItem(itemPosition) is StickyHeaderItem
}
