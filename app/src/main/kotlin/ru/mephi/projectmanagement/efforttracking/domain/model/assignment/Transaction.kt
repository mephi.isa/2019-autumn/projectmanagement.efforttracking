package ru.mephi.projectmanagement.efforttracking.domain.model.assignment

import org.joda.time.DateTime

data class Transaction(
    val id: Int,
    val date: DateTime,
    val workingHours: Float,
    val comment: String,
    val workType: WorkType? = WorkType.STANDARD,
    val workRate: WorkRate? = WorkRate.X_1
)
