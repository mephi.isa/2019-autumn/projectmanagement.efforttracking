package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state

enum class LoadingWithErrorScreenState {
    CONTENT,
    LOADING,
    ERROR
}
