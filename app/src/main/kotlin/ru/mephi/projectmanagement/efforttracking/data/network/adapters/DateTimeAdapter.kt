package ru.mephi.projectmanagement.efforttracking.data.network.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class DateTimeAdapter {

    private val dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")

    @ToJson
    fun toJson(dateTime: DateTime?): String? {
        return dateTime?.let { dateTimeFormat.print(it) }
    }

    @FromJson
    fun fromJson(dateTime: String?): DateTime? {
        if (dateTime == null) return null

        return dateTimeFormat.parseDateTime(dateTime)
    }
}
