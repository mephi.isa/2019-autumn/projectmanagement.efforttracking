package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_transaction.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.domain.extension.floatToTime
import ru.mephi.projectmanagement.efforttracking.domain.extension.toFormattedTime
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction

data class TransactionItem(
    private val transaction: Transaction,
    private val assignmentStatus: AssignmentStatus,
    private val onEditClick: (Transaction) -> Unit
) : Item() {

    override fun getId() = transaction.id.toLong()

    override fun getLayout(): Int = R.layout.item_transaction

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder) {
            item_transaction_text_view_time.text = transaction.workingHours.floatToTime().toFormattedTime()
            item_transaction_text_view_work_rate.text = transaction.workRate?.value
            item_transaction_text_view_work_type.text = transaction.workType?.value
            item_transaction_text_view_comment.text = transaction.comment

            when (assignmentStatus) {
                AssignmentStatus.MUTABLE -> {
                    item_transaction_button_edit.isVisible = true
                    item_transaction_button_edit.setOnClickListener { onEditClick(transaction) }
                }
                AssignmentStatus.IMMUTABLE -> {
                    item_transaction_button_edit.isGone = true
                }
            }

        }
    }
}
