package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import androidx.lifecycle.MutableLiveData
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import ru.mephi.projectmanagement.efforttracking.app.extension.onNext
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.SchedulersProvider
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.scheduleIoToUi
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.TransactionsViewState.*
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import timber.log.Timber

class TransactionsViewModel @AssistedInject constructor(
    @Assisted private val assignmentId: Int,
    private val assignmentRepository: AssignmentRepository,
    private val schedulers: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val TAG = "TransactionsVM"
    }

    val state = MutableLiveData<TransactionsViewState>()

    private var contentState = Content()
        set(value) {
            field = value
            state.onNext(field)
        }

    init {
        loadTransactions()
    }

    private fun loadTransactions() {
        assignmentRepository
            .getAssignment(assignmentId)
            .map { it.transactionList to it.assignmentStatus }
            .scheduleIoToUi(schedulers)
            .doOnSubscribe { state.onNext(Loading) }
            .safeSubscribe(
                onSuccess = { (transactions, assignmentStatus) ->
                    contentState = contentState.copy(
                        transactions = transactions,
                        assignmentStatus = assignmentStatus
                    )
                },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    commands.onNext(ShowSnackbarError(message))
                    state.onNext(Error(throwable))
                }
            )
    }

    @AssistedInject.Factory
    interface Factory {
        fun create(assignmentId: Int): TransactionsViewModel
    }

}
