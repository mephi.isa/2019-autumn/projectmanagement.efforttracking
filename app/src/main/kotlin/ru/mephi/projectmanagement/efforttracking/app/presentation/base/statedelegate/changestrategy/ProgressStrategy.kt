package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.changestrategy

import android.view.View
import com.redmadrobot.lib.sd.base.ShowOnEnterGoneOnExitStrategy
import com.redmadrobot.lib.sd.base.State
import kotlinx.android.synthetic.main.progress.view.*

class ProgressStrategy<T : Enum<T>>(private val progress: View) : ShowOnEnterGoneOnExitStrategy<T>() {

    override fun onStateEnter(state: State<T>, prevState: State<T>?) {
        super.onStateEnter(state, prevState)
        progress.progress_lottie.playAnimation()
    }

    override fun onStateExit(state: State<T>, nextState: State<T>?) {
        progress.progress_lottie.cancelAnimation()
        super.onStateExit(state, nextState)
    }
}
