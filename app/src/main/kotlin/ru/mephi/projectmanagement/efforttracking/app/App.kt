package ru.mephi.projectmanagement.efforttracking.app

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.AppComponent
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.DaggerAppComponent
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module.ContextModule
import ru.mephi.projectmanagement.efforttracking.domain.infrastructure.PrettyLoggingTree
import timber.log.Timber

class App : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        JodaTimeAndroid.init(this)

        Timber.plant(PrettyLoggingTree(this))
    }

}
