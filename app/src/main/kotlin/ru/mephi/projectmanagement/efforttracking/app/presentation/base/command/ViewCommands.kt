package ru.mephi.projectmanagement.efforttracking.app.presentation.base.command

import org.joda.time.DateTime

interface ViewCommand

data class ShowSnackbarMessage(val message: String) : ViewCommand

data class ShowSnackbarError(val message: String) : ViewCommand

class Back : ViewCommand
class Next : ViewCommand

class ShowDatePicker(val startDate: DateTime, val endDate: DateTime, val currentDate: DateTime?) : ViewCommand
