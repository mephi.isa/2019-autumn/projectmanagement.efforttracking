package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate

import android.view.View
import com.redmadrobot.lib.sd.base.State
import com.redmadrobot.lib.sd.base.StateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.ErrorScreenState

class ErrorStateDelegate(
    contentView: List<View>,
    errorView: View
) : StateDelegate<ErrorScreenState>(
    State(ErrorScreenState.CONTENT, contentView),
    State(ErrorScreenState.ERROR, listOf(errorView))
) {

    fun showContent() {
        currentState = ErrorScreenState.CONTENT
    }

    fun showError() {
        currentState = ErrorScreenState.ERROR
    }
}
