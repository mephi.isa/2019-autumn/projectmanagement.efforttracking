@file:Suppress("unused", "TooManyFunctions")

package ru.mephi.projectmanagement.efforttracking.app.extension

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView

fun View.setKeyboardFocusViewImmediate() {
    requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun Activity.hideKeyboard() {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    val view = this.currentFocus?.let { it } ?: View(this)
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Метод удаляет adapter из [RecyclerView], чтобы избежать утечки памяти.
 *
 * Подробнее:
 * https://github.com/airbnb/epoxy/wiki/Avoiding-Memory-Leaks
 * https://stackoverflow.com/a/46957469/5484505
 */
fun RecyclerView.removeAdapterOnViewDetachedFromWindow() {
    this.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
        override fun onViewAttachedToWindow(v: View) {
            // Do nothing
        }

        override fun onViewDetachedFromWindow(v: View) {
            this@removeAdapterOnViewDetachedFromWindow.adapter = null
        }
    })
}

fun EditText.setOnTextChangeListener(listener: (String) -> Unit): TextWatcher {
    return object : TextWatcher {

        override fun afterTextChanged(s: Editable) {
            /* ignored */
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            /* ignored */
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            listener.invoke(s.toString())
        }

    }.apply { addTextChangedListener(this) }
}

fun EditText.setOnDoneActionListener(listener: (view: EditText) -> Boolean) {
    this.setOnEditorActionListener { _, actionId, _ ->
        when (actionId) {
            EditorInfo.IME_ACTION_DONE -> listener.invoke(this)
            else -> false
        }
    }
}
