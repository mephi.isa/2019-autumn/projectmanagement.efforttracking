package ru.mephi.projectmanagement.efforttracking.data.repository

import io.reactivex.Single
import ru.mephi.projectmanagement.efforttracking.data.network.ServerAPI
import ru.mephi.projectmanagement.efforttracking.data.network.request.auth.LoginRequest
import ru.mephi.projectmanagement.efforttracking.domain.model.User
import ru.mephi.projectmanagement.efforttracking.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(private val serverAPI: ServerAPI) : UserRepository {

    private var user: User? = null

    override fun authorize(login: String, password: String): Single<User> {
        return serverAPI.login(LoginRequest(login, password)).doOnSuccess { user = it }
    }

    override fun getUser() = user

    override fun logout() {
        user = null
    }
}
