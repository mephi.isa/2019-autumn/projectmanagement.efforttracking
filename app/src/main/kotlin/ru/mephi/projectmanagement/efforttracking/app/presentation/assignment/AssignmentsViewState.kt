package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment

import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment

sealed class AssignmentsViewState {
    object Loading : AssignmentsViewState()

    data class Content(val assignments: List<Assignment> = emptyList()) : AssignmentsViewState()

    data class Error(val error: Throwable) : AssignmentsViewState()
}

