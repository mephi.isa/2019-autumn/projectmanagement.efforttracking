package ru.mephi.projectmanagement.efforttracking.app.presentation.auth

import android.content.Context
import android.os.Bundle
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlinx.android.synthetic.main.progress.view.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.extension.*
import ru.mephi.projectmanagement.efforttracking.app.presentation.auth.AuthViewState.Content
import ru.mephi.projectmanagement.efforttracking.app.presentation.auth.AuthViewState.Loading
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.Next
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarMessage
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ViewCommand
import javax.inject.Inject
import javax.inject.Provider

class AuthFragment : Fragment(R.layout.fragment_auth) {

    @Inject
    lateinit var viewModelProvider: Provider<AuthViewModel>

    private val viewModel by viewModels { viewModelProvider.get() }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as App).appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initViews()

        observe(viewModel.state, ::handleState)
        observe(viewModel.commands, ::handleCommand)
    }

    private fun initViews() {
        fragment_auth_edit_text_login.setOnTextChangeListener { viewModel.onLoginChanged(it) }
        fragment_auth_edit_text_password.setOnTextChangeListener { viewModel.onPasswordChanged(it) }

        fragment_auth_button_enter.setOnClickListener { onEnterButtonClick() }
    }

    private fun onEnterButtonClick() {
        requireActivity().hideKeyboard()
        viewModel.onEnterButtonClick()
    }

    private fun handleCommand(command: ViewCommand) {
        when (command) {

            is ShowSnackbarError -> {
                (requireActivity() as BaseActivity).showError(messageText = command.message)
            }
            is ShowSnackbarMessage -> {
                (requireActivity() as BaseActivity).showMessage(messageText = command.message)
            }
            is Next -> {
                val action = AuthFragmentDirections.onLoginSuccess()
                requireView().findNavController().navigate(action)
            }
        }
    }

    private fun handleState(state: AuthViewState) {
        when (state) {
            Loading -> showLoading()
            is Content -> handleContentState(state)
        }
    }

    private fun showLoading() {
        fragment_auth_progress.isVisible = true
        fragment_auth_progress.progress_lottie.playAnimation()
    }

    private fun hideLoading() {
        fragment_auth_progress.progress_lottie.cancelAnimation()
        fragment_auth_progress.isGone = true
    }

    private fun handleContentState(state: Content) {
        hideLoading()

        with(fragment_auth_edit_text_login) {
            if (text.toString() != state.login) setText(state.login)
        }

        with(fragment_auth_edit_text_password) {
            if (text.toString() != state.password) setText(state.password)
        }

        fragment_auth_edit_text_password.setOnDoneActionListener {
            if (state.isEnterButtonEnabled) {
                onEnterButtonClick()
                true
            } else {
                false
            }
        }

        fragment_auth_button_enter.isEnabled = state.isEnterButtonEnabled
    }

}
