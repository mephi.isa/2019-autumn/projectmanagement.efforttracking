package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Section
import kotlinx.android.synthetic.main.fragment_assignments.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.extension.isNetworkException
import ru.mephi.projectmanagement.efforttracking.app.extension.observe
import ru.mephi.projectmanagement.efforttracking.app.extension.removeAdapterOnViewDetachedFromWindow
import ru.mephi.projectmanagement.efforttracking.app.extension.viewModels
import ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.AssignmentsViewState.*
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarMessage
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ViewCommand
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.SimpleItem
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate.LoadingWithErrorStateDelegate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import javax.inject.Inject
import javax.inject.Provider

class AssignmentsFragment : Fragment(R.layout.fragment_assignments) {

    private val topAnchorView = SimpleItem(R.layout.view_empty)
    private val assignmentsSection = Section()

    @Inject
    lateinit var viewModelProvider: Provider<AssignmentsViewModel>

    private val viewModel by viewModels { viewModelProvider.get() }

    private lateinit var screenState: LoadingWithErrorStateDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as App).appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(viewModel.state, ::handleState)
        observe(viewModel.commands, ::handleCommand)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        fragment_assignments_recycler.removeAdapterOnViewDetachedFromWindow()
        super.onDestroyView()
    }

    private fun initViews() {
        screenState = LoadingWithErrorStateDelegate(
            contentView = fragment_assignments_recycler,
            loadingView = fragment_assignments_container_progress,
            errorView = fragment_assignments_error_view
        )

        with(fragment_assignments_recycler) {
            layoutManager = LinearLayoutManager(context)
            adapter = GroupAdapter<GroupieViewHolder>().apply {
                add(topAnchorView)
                add(assignmentsSection)
            }
        }

        fragment_assignments_error_view.setOnRetryClickListener { viewModel.onRetryClick() }
    }

    private fun showLoading() {
        screenState.showLoading()
    }

    private fun handleState(viewState: AssignmentsViewState) {
        when (viewState) {
            Loading -> showLoading()
            is Content -> {
                if (viewState.assignments.isEmpty()) handleStub() else handleContent(viewState.assignments)
            }
            is Error -> handleError(viewState.error)
        }

    }

    private fun handleContent(assignments: List<Assignment>) {
        val items = assignments.map { assignment ->
            AssignmentItem(
                assignment = assignment,
                onClick = ::onAssignmentClick
            )
        }

        fragment_assignments_recycler.post {
            assignmentsSection.update(items)
        }

        screenState.showContent()
    }

    private fun handleStub() {
        fragment_assignments_error_view.showStub()
        screenState.showError()
    }

    private fun handleError(error: Throwable) {
        fragment_assignments_error_view.showError(error.isNetworkException())
        screenState.showError()
    }

    private fun handleCommand(command: ViewCommand) {
        when (command) {
            is ShowSnackbarMessage -> (requireActivity() as BaseActivity).showMessage(messageText = command.message)
            is ShowSnackbarError -> (requireActivity() as BaseActivity).showError(messageText = command.message)
        }
    }

    private fun onAssignmentClick(assignment: Assignment) {
        val action = AssignmentsFragmentDirections.onAssignmentClick(assignment.id)
        requireView().findNavController().navigate(action)
    }

}
