package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate

import android.view.View
import com.redmadrobot.lib.sd.base.State
import com.redmadrobot.lib.sd.base.StateChangeStrategy
import com.redmadrobot.lib.sd.base.StateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.changestrategy.ProgressStrategy
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.LoadingWithStubScreenState
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.LoadingWithStubScreenState.*

class LoadingWithStubStateDelegate(
    loadingViews: List<View>,
    loadingStateChangeStrategy: StateChangeStrategy<LoadingWithStubScreenState> = ProgressStrategy(loadingViews.last()),
    contentViews: List<View>,
    stubViews: List<View>,
    errorViews: List<View>
) : StateDelegate<LoadingWithStubScreenState>(
    State(LOADING, loadingViews, loadingStateChangeStrategy),
    State(CONTENT, contentViews),
    State(STUB, stubViews),
    State(ERROR, errorViews)
) {

    constructor(
        loadingView: View,
        loadingStateChangeStrategy: StateChangeStrategy<LoadingWithStubScreenState> = ProgressStrategy(loadingView),
        contentView: View,
        stubView: View,
        errorView: View
    ) : this(
        listOf(loadingView),
        loadingStateChangeStrategy,
        listOf(contentView),
        listOf(stubView),
        listOf(errorView)
    )

    fun showLoading() {
        currentState = LOADING
    }

    fun showContent() {
        currentState = CONTENT
    }

    fun showStub() {
        currentState = STUB
    }

    fun showError() {
        currentState = ERROR
    }
}
