package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import androidx.lifecycle.MutableLiveData
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.app.extension.onNext
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.SchedulersProvider
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.scheduleIoToUi
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.Back
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowDatePicker
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.EditTransactionViewState.Content
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.EditTransactionViewState.Loading
import ru.mephi.projectmanagement.efforttracking.domain.extension.floatToTime
import ru.mephi.projectmanagement.efforttracking.domain.extension.timeToFloat
import ru.mephi.projectmanagement.efforttracking.domain.extension.toFormattedTime
import ru.mephi.projectmanagement.efforttracking.domain.extension.toTime
import ru.mephi.projectmanagement.efforttracking.domain.interactor.AssignmentInteractor
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkRate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkType
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import timber.log.Timber
import java.util.concurrent.TimeUnit

class EditTransactionViewModel @AssistedInject constructor(
    @Assisted private val assignmentId: Int,
    @Assisted private val transactionId: Int,
    private val assignmentInteractor: AssignmentInteractor,
    private val assignmentRepository: AssignmentRepository,
    private val schedulers: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val TAG = "EditTransactionVM"
        private const val TIME_STRING_LENGTH = 5
    }

    val state = MutableLiveData<EditTransactionViewState>()

    private var contentState = Content()
        set(value) {
            field = value
            state.onNext(field)
        }

    init {
        loadTransaction()
    }

    fun onTimeChanged(time: String) {
        if (time == contentState.time) return

        if (time.length == TIME_STRING_LENGTH) {
            try {
                time.toTime()
            } catch (e: IllegalArgumentException) {
                commands.onNext(ShowSnackbarError("Введено не корректное время"))
                return
            }
        }

        contentState = contentState.copy(
            time = time,
            saveButtonEnabled = areParamsValid(time = time)
        )
    }

    fun onDateClick() {
        assignmentRepository
            .getAssignment(assignmentId)
            .scheduleIoToUi(schedulers)
            .safeSubscribe(
                onSuccess = { assignment ->
                    commands.onNext(
                        ShowDatePicker(
                            assignment.startDate,
                            assignment.endDate,
                            contentState.date
                        )
                    )
                },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    contentState = contentState
                    commands.onNext(ShowSnackbarError(message))
                }
            )

    }

    fun onDateChanged(dateTime: DateTime) {
        if (dateTime == contentState.date) return

        contentState = contentState.copy(
            date = dateTime,
            saveButtonEnabled = areParamsValid(date = dateTime)
        )
    }

    fun onWorkRateChanged(workRate: WorkRate) {
        if (workRate == contentState.workRate) return

        contentState = contentState.copy(
            workRate = workRate,
            saveButtonEnabled = areParamsValid()
        )
    }

    fun onWorkTypeChanged(workType: WorkType) {
        if (workType == contentState.workType) return

        contentState = contentState.copy(
            workType = workType,
            saveButtonEnabled = areParamsValid()
        )
    }

    fun onCommentChanged(comment: String) {
        if (comment == contentState.comment) return

        contentState = contentState.copy(
            comment = comment,
            saveButtonEnabled = areParamsValid(comment = comment)
        )
    }

    fun onSaveClick() {
        val workingHours = contentState.time.timeToFloat()
        if (workingHours == -1f) {
            commands.onNext(ShowSnackbarError("Введено не корректнон время"))
            return
        }

        if (transactionId == -1) {
            assignmentInteractor
                .addWorkingHours(
                    assignmentId = assignmentId,
                    date = contentState.date!!,
                    workingHours = workingHours,
                    workRate = contentState.workRate,
                    workType = contentState.workType,
                    comment = contentState.comment
                )
                .scheduleIoToUi(schedulers)
                .doOnSubscribe { state.run { onNext(Loading) } }
                .safeSubscribe(
                    onSuccess = {
                        contentState = contentState
                        commands.onNext(Back())
                    },
                    onError = { throwable, message ->
                        Timber.tag(TAG).e(throwable)
                        contentState = contentState
                        commands.onNext(ShowSnackbarError(message))
                    }
                )
        } else {
            assignmentInteractor
                .updateWorkingHours(
                    assignmentId = assignmentId,
                    transactionId = transactionId,
                    date = contentState.date!!,
                    workingHours = workingHours,
                    workRate = contentState.workRate,
                    workType = contentState.workType,
                    comment = contentState.comment
                )
                // FIXME Удалить задержку
                .delay(2, TimeUnit.SECONDS)
                .scheduleIoToUi(schedulers)
                .doOnSubscribe { state.run { onNext(Loading) } }
                .safeSubscribe(
                    onSuccess = {
                        contentState = contentState
                        commands.onNext(Back())
                    },
                    onError = { throwable, message ->
                        Timber.tag(TAG).e(throwable)
                        contentState = contentState
                        commands.onNext(ShowSnackbarError(message))
                    }
                )
        }
    }

    private fun loadTransaction() {
        if (transactionId == -1) return

        assignmentRepository
            .getAssignment(assignmentId)
            .map { it.transactionList }
            .map { transactions -> transactions.first { it.id == transactionId } }
            .scheduleIoToUi(schedulers)
            .safeSubscribe(
                onSuccess = { transaction ->
                    contentState = contentState.copy(
                        time = transaction.workingHours.floatToTime().toFormattedTime(),
                        date = transaction.date,
                        workRate = transaction.workRate ?: WorkRate.X_1,
                        workType = transaction.workType ?: WorkType.STANDARD,
                        comment = transaction.comment
                    )
                },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    contentState = contentState
                    commands.onNext(ShowSnackbarError(message))
                }
            )
    }

    private fun areParamsValid(
        time: String = contentState.time,
        date: DateTime? = contentState.date,
        comment: String = contentState.comment
    ): Boolean {
        return time.length == TIME_STRING_LENGTH && date != null && comment.isNotBlank()
    }

    @AssistedInject.Factory
    interface Factory {
        fun create(assignmentId: Int, transactionId: Int): EditTransactionViewModel
    }

}
