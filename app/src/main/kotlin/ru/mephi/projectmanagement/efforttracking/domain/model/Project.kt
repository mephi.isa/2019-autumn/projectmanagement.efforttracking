package ru.mephi.projectmanagement.efforttracking.domain.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Project(
    val name: String,
    val description: String? = null
)
