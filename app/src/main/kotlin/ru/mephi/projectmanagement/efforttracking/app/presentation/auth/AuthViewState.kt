package ru.mephi.projectmanagement.efforttracking.app.presentation.auth

sealed class AuthViewState {

    object Loading : AuthViewState()

    data class Content(
        val login: String = "",
        val password: String = "",
        val isEnterButtonEnabled: Boolean = false
    ) : AuthViewState()

}
