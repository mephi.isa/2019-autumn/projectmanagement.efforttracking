package ru.mephi.projectmanagement.efforttracking.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.domain.model.Project
import ru.mephi.projectmanagement.efforttracking.domain.model.Task
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@Suppress("LongMethod", "MagicNumber")
class AssignmentRepositoryMock @Inject constructor() : AssignmentRepository {

    private val assignments: MutableList<Assignment> by lazy { generateMockList() }

    override fun getAssignments(): Single<List<Assignment>> {
        return Single.just(assignments as List<Assignment>).delay(3, TimeUnit.SECONDS)
    }

    override fun getAssignment(id: Int): Single<Assignment> {
        return getAssignments()
            .flattenAsObservable { it }
            .filter { it.id == id }
            .firstOrError()
            .delay(1, TimeUnit.SECONDS)
    }

    override fun setAssignment(assignment: Assignment): Completable {
        return Completable
            .fromCallable {
                val index = assignments.indexOfFirst { it.id == assignment.id }
                assignments.set(index, assignment)
            }
            .delay(1, TimeUnit.SECONDS)
    }

    private fun generateMockList(): MutableList<Assignment> {
        val project = Project(
            name = "Project name 1",
            description = "Project description 1"
        )

        val task1 = Task(
            name = "Уход за котом",
            project = project,
            startDate = DateTime.now().minusMonths(2),
            endDate = DateTime.now().plusMonths(2),
            description = "Task description 1"
        )

        val task2 = Task(
            name = "Уход за машиной",
            project = project,
            startDate = DateTime.now().minusMonths(1),
            endDate = DateTime.now().plusMonths(1),
            description = "Task description 2"
        )

        val task3 = Task(
            name = "Уход за домом",
            project = project,
            startDate = DateTime.now().minusMonths(1),
            endDate = DateTime.now().plusMonths(1),
            description = "Task description 3"
        )

        val task4 = Task(
            name = "Переезд",
            project = project,
            startDate = DateTime.now().minusMonths(1),
            endDate = DateTime.now().plusMonths(1),
            description = "Task description 4"
        )

        return mutableListOf(
            Assignment(
                id = 1,
                title = "Помыть кота",
                description = "При температуре 37-38С, подвергнуть кота водным процедурам.",
                task = task1,
                startDate = DateTime.now().minusDays(24),
                endDate = DateTime.now().plusDays(24),
                estimatedHours = 999f,
                assignmentStatus = AssignmentStatus.MUTABLE
            ).apply {
                transactionList.addAll(
                    listOf(
                        Transaction(
                            1,
                            DateTime.now().minusDays(23),
                            1.75f,
                            "Какая-то работа 1"
                        ),
                        Transaction(
                            2,
                            DateTime.now().minusDays(23),
                            2.50f,
                            "Какая-то работа 2"
                        ),
                        Transaction(
                            3,
                            DateTime.now().minusDays(23),
                            0.45f,
                            "Какая-то работа 3"
                        ),
                        Transaction(
                            4,
                            DateTime.now().minusDays(22),
                            1.0f,
                            "Какая-то работа 4"
                        ),
                        Transaction(
                            5,
                            DateTime.now().minusDays(22),
                            5.0f,
                            "Какая-то работа 5"
                        ),
                        Transaction(
                            6,
                            DateTime.now().minusDays(21),
                            7.95f,
                            "Какая-то работа 6"
                        )
                    )
                )
            },
            Assignment(
                id = 2,
                title = "Помыть машину",
                description = "Assignment description for task 2",
                task = task2,
                startDate = DateTime.now().minusDays(16),
                endDate = DateTime.now().plusDays(16),
                estimatedHours = 20f,
                assignmentStatus = AssignmentStatus.IMMUTABLE
            ),
            Assignment(
                id = 3,
                title = "Покрасить дом",
                description = "Assignment description for task 3",
                task = task3,
                startDate = DateTime.now().minusDays(8),
                endDate = DateTime.now().plusDays(8),
                estimatedHours = 40f,
                assignmentStatus = AssignmentStatus.MUTABLE
            ),
            Assignment(
                id = 4,
                title = "Разобрать коробки",
                description = "Assignment description for task 4",
                task = task4,
                startDate = DateTime.now().minusDays(2),
                endDate = DateTime.now().plusDays(2),
                estimatedHours = 2f,
                assignmentStatus = AssignmentStatus.MUTABLE
            )
        )
    }

}
