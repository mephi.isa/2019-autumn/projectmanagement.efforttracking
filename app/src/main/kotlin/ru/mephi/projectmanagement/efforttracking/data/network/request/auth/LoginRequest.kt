package ru.mephi.projectmanagement.efforttracking.data.network.request.auth

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class LoginRequest(

    @Json(name = "login")
    val login: String,

    @Json(name = "password")
    val password: String
)
