package ru.mephi.projectmanagement.efforttracking.app.infrastructure.di

import dagger.Component
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module.AppModule
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module.ContextModule
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module.NetworkModule
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module.ViewModelAssistedFactoriesModule
import ru.mephi.projectmanagement.efforttracking.app.presentation.AppActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.AssignmentsFragment
import ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.detail.AssignmentDetailFragment
import ru.mephi.projectmanagement.efforttracking.app.presentation.auth.AuthFragment
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.EditTransactionFragment
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.TransactionsFragment
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        ContextModule::class,
        ViewModelAssistedFactoriesModule::class
    ]
)
interface AppComponent {

    fun inject(activity: AppActivity)

    fun inject(fragment: AuthFragment)
    fun inject(fragment: AssignmentsFragment)
    fun inject(fragment: AssignmentDetailFragment)
    fun inject(fragment: TransactionsFragment)
    fun inject(fragment: EditTransactionFragment)

}
