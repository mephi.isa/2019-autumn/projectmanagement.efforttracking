package ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module

import android.content.Context
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.mephi.projectmanagement.efforttracking.BuildConfig
import ru.mephi.projectmanagement.efforttracking.data.network.MoshiFactory
import ru.mephi.projectmanagement.efforttracking.data.network.ServerAPI

@Module
class NetworkModule {

    companion object {
        private const val SERVER_URL = "https://4a4578dc.ngrok.io/api/"
    }

    @Provides
    fun serverApi(retrofit: Retrofit): ServerAPI {
        return retrofit.create<ServerAPI>(ServerAPI::class.java)
    }

    @Provides
    fun retrofit(
        okHttpClient: OkHttpClient,
        moshiFactory: MoshiFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(SERVER_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshiFactory.networkMoshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    fun okHttpClient(
        chuckInterceptor: ChuckInterceptor,
        loggingInterceptor: LoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(chuckInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    fun chuckInterceptor(context: Context): ChuckInterceptor {
        return ChuckInterceptor(context)
    }

    @Provides
    fun loggingInterceptor(): LoggingInterceptor {
        return LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .request("Request")
            .response("Response")
            .build()
    }

}
