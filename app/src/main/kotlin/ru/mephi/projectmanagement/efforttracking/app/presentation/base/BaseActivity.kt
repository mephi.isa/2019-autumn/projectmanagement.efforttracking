package ru.mephi.projectmanagement.efforttracking.app.presentation.base

import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import ru.mephi.projectmanagement.efforttracking.R

abstract class BaseActivity : AppCompatActivity() {

    private val mainView by lazy { window.decorView }

    fun showMessage(messageText: String) {
        val snack = createSnack(
            messageText = messageText,
            backgroundColor = R.color.primary_dark,
            textColor = R.color.text_primary
        )

        snack?.show()
    }

    fun showError(messageText: String) {
        val snack = createSnack(
            messageText = messageText,
            backgroundColor = R.color.error,
            textColor = R.color.text_primary
        )

        snack?.show()
    }

    @Suppress("LongParameterList")
    private fun createSnack(
        messageText: String,
        @ColorRes backgroundColor: Int,
        @ColorRes textColor: Int,
        containerResId: Int = android.R.id.content,
        duration: Int = Snackbar.LENGTH_LONG
    ): Snackbar? {

        val viewGroup = mainView.findViewById(containerResId) as ViewGroup?

        return viewGroup?.let {
            Snackbar
                .make(viewGroup, messageText, duration)
                .apply {
                    setBackgroundTint(ContextCompat.getColor(view.context, backgroundColor))
                    setTextColor(ContextCompat.getColor(view.context, textColor))
                }
        }
    }
}
