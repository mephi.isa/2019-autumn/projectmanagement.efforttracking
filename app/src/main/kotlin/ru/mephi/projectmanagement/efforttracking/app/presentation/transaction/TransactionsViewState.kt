package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction

sealed class TransactionsViewState {
    object Loading : TransactionsViewState()

    data class Content(
        val transactions: List<Transaction> = emptyList(),
        val assignmentStatus: AssignmentStatus = AssignmentStatus.IMMUTABLE
    ) : TransactionsViewState()

    data class Error(val error: Throwable) : TransactionsViewState()
}
