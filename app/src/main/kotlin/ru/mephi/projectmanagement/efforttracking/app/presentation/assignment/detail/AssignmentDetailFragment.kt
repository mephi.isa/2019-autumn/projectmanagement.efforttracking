package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.detail

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_assignment_detail.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.extension.observe
import ru.mephi.projectmanagement.efforttracking.app.extension.viewModels
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarMessage
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ViewCommand
import ru.mephi.projectmanagement.efforttracking.domain.extension.toFormattedDate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import javax.inject.Inject


class AssignmentDetailFragment : Fragment(R.layout.fragment_assignment_detail) {

    val args: AssignmentDetailFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: AssignmentDetailViewModel.Factory

    private val viewModel by viewModels { viewModelFactory.create(args.assignmentId) }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as App).appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(viewModel.state, this::render)
        observe(viewModel.commands, ::handleCommand)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView() {
        fragment_assignment_detail_toolbar.setNavigationOnClickListener {
            requireView().findNavController().navigateUp()
        }
    }

    private fun render(assignment: Assignment) {
        fragment_assignment_detail_text_view_title.text = assignment.title
        fragment_assignment_detail_text_view_status.text = assignment.assignmentStatus.value
        fragment_assignment_detail_text_view_logged_hours.text = assignment.loggedHours().toString()
        fragment_assignment_detail_text_view_estimated_hours.text = assignment.estimatedHours.toString()
        fragment_assignment_detail_text_view_date_interval.text = requireContext().getString(
            R.string.date_interval,
            assignment.startDate.toFormattedDate(),
            assignment.endDate.toFormattedDate()
        )
        fragment_assignment_detail_text_view_description.text = assignment.description
        fragment_assignment_detail_container_logged_hours.setOnClickListener {
            val action = AssignmentDetailFragmentDirections.onTransactionsClick(args.assignmentId)
            requireView().findNavController().navigate(action)
        }
    }

    private fun handleCommand(command: ViewCommand) {
        when (command) {
            is ShowSnackbarMessage -> (requireActivity() as BaseActivity).showMessage(messageText = command.message)
            is ShowSnackbarError -> (requireActivity() as BaseActivity).showError(messageText = command.message)
        }
    }

}
