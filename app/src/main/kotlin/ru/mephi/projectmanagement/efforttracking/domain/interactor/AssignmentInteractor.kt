package ru.mephi.projectmanagement.efforttracking.domain.interactor

import io.reactivex.Single
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.AssignmentStatus
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Transaction
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkRate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkType
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import javax.inject.Inject
import kotlin.math.min

class AssignmentInteractor @Inject constructor(
    private val assignmentRepository: AssignmentRepository
) {

    companion object {
        private const val HOURS_IN_DAY = 24f
        private const val TWO_WEEKS_IN_DAYS = 14
    }

    fun addWorkingHours(
        assignmentId: Int,
        date: DateTime,
        workingHours: Float,
        comment: String,
        workType: WorkType? = null,
        workRate: WorkRate? = null
    ): Single<Transaction> {

        return assignmentRepository
            .getAssignment(assignmentId)
            .flatMap { assignment ->

                val maxAssignmentId = assignment
                    .transactionList
                    .maxBy { assignmentId }
                    ?.id ?: 0

                check(date.isAfter(assignment.startDate) && date.isBefore(assignment.endDate)) {
                    "The transaction date is outside the assignment dates interval"
                }

                val dateUntilChangeAllowed = date.plusDays(TWO_WEEKS_IN_DAYS)
                check(DateTime.now().isBefore(dateUntilChangeAllowed)) {
                    "Transaction is outdated"
                }

                val transaction = Transaction(
                    id = maxAssignmentId + 1,
                    date = date,
                    workingHours = min(workingHours, HOURS_IN_DAY),
                    comment = comment,
                    workType = workType,
                    workRate = workRate
                )

                val sumOfHoursInThisDay = assignment
                    .transactionList
                    .filter { it.date.toLocalDate() == date.toLocalDate() }
                    .map { it.workingHours }
                    .sum()

                require(sumOfHoursInThisDay + transaction.workingHours <= HOURS_IN_DAY) {
                    "The number of working hours per day more than 24 hours"
                }

                assignment.transactionList.add(transaction)
                assignment.transactionList.sortBy { it.date }

                assignmentRepository
                    .setAssignment(assignment)
                    .toSingleDefault(transaction)
            }
    }

    fun updateWorkingHours(
        assignmentId: Int,
        transactionId: Int,
        date: DateTime,
        workingHours: Float,
        comment: String? = null,
        workType: WorkType? = null,
        workRate: WorkRate? = null
    ): Single<Transaction> {

        return assignmentRepository
            .getAssignment(assignmentId)
            .flatMap { assignment ->

                check(assignment.assignmentStatus == AssignmentStatus.MUTABLE) {
                    "Transaction is immutable"
                }

                check(date.isAfter(assignment.startDate) && date.isBefore(assignment.endDate)) {
                    "The transaction date is outside the assignment dates interval"
                }

                val dateUntilChangeAllowed = date.plusDays(TWO_WEEKS_IN_DAYS)
                check(DateTime.now().isBefore(dateUntilChangeAllowed)) {
                    "Transaction is outdated"
                }

                val currentTransactionIndex = assignment
                    .transactionList
                    .indexOfFirst { it.id == transactionId }

                require(currentTransactionIndex != -1) {
                    "Transaction does not exist"
                }

                val currentTransaction = assignment.transactionList[currentTransactionIndex]

                val updatedTransaction = currentTransaction.copy(
                    workingHours = workingHours,
                    comment = comment ?: currentTransaction.comment,
                    workType = workType ?: currentTransaction.workType,
                    workRate = workRate ?: currentTransaction.workRate
                )

                assignment.transactionList[currentTransactionIndex] = updatedTransaction
                assignment.transactionList.sortBy { it.date }
                assignmentRepository
                    .setAssignment(assignment)
                    .toSingleDefault(updatedTransaction)
            }
    }

}
