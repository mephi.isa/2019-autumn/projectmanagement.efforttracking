package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state

enum class LoadingWithStubScreenState {
    LOADING,
    STUB,
    ERROR,
    CONTENT
}
