package ru.mephi.projectmanagement.efforttracking.domain.model.assignment

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.domain.model.Task

@JsonClass(generateAdapter = true)
data class Assignment(

    @Json(name = "taskNumber")
    val id: Int,

    @Json(name = "name")
    val title: String,

    @Json(name = "description")
    val description: String,

    val task: Task? = null,

    @Json(name = "planned_start_dt")
    val startDate: DateTime,

    @Json(name = "planned_close_dt")
    val endDate: DateTime,

    @Json(name = "apprasial")
    val estimatedHours: Float,

    val assignmentStatus: AssignmentStatus = AssignmentStatus.MUTABLE

) {

    val transactionList: MutableList<Transaction> = mutableListOf()

    fun loggedHours() = transactionList.map { it.workingHours }.sum()

    fun getRemainingHours() = estimatedHours - loggedHours()

}
