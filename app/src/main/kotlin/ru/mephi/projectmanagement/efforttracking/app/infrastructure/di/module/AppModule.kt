package ru.mephi.projectmanagement.efforttracking.app.infrastructure.di.module

import dagger.Binds
import dagger.Module
import ru.mephi.projectmanagement.efforttracking.data.repository.AssignmentRepositoryImpl
import ru.mephi.projectmanagement.efforttracking.data.repository.UserRepositoryImpl
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import ru.mephi.projectmanagement.efforttracking.domain.repository.UserRepository
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun provideUserRepository(repository: UserRepositoryImpl): UserRepository

    @Binds
    @Singleton
    abstract fun provideAssignmentRepository(repository: AssignmentRepositoryImpl): AssignmentRepository

}
