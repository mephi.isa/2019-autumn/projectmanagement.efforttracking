package ru.mephi.projectmanagement.efforttracking.data.repository

import io.reactivex.Single
import ru.mephi.projectmanagement.efforttracking.domain.model.User
import ru.mephi.projectmanagement.efforttracking.domain.repository.UserRepository

class UserRepositoryMock : UserRepository {

    private var user: User? = null

    override fun authorize(login: String, password: String): Single<User> {
        return Single.just(User(
            id = 1,
            fio = login,
            role = "Manager",
            managerId = -1
        ).also { user = it })
    }

    override fun getUser() = user

    override fun logout() {
        user = null
    }
}
