@file:Suppress("PackageName")

package ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.sticky_header

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder

interface StickyHeaderItem {

    fun bindToViewHolder(viewHolder: GroupieViewHolder)
}
