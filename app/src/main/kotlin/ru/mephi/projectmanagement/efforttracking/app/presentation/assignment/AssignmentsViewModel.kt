package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment

import androidx.lifecycle.MutableLiveData
import ru.mephi.projectmanagement.efforttracking.app.extension.onNext
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.SchedulersProvider
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.scheduleIoToUi
import ru.mephi.projectmanagement.efforttracking.app.presentation.assignment.AssignmentsViewState.*
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import timber.log.Timber
import javax.inject.Inject

class AssignmentsViewModel @Inject constructor(
    private val assignmentRepository: AssignmentRepository,
    private val schedulers: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val TAG = "AssignmentsVM"
    }

    val state = MutableLiveData<AssignmentsViewState>()

    private var contentState = Content()
        set(value) {
            field = value
            state.onNext(field)
        }

    init {
        loadAssignments()
    }

    fun onRetryClick() {
        loadAssignments()
    }

    private fun loadAssignments() {
        assignmentRepository
            .getAssignments()
            .scheduleIoToUi(schedulers)
            .doOnSubscribe { state.onNext(Loading) }
            .safeSubscribe(
                onSuccess = { contentState = contentState.copy(assignments = it) },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    state.onNext(Error(throwable))
                }
            )
    }

}
