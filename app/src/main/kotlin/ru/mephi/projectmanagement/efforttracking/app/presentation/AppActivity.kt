package ru.mephi.projectmanagement.efforttracking.app.presentation

import android.os.Bundle
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import javax.inject.Inject

class AppActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: AppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as App).appComponent.inject(this)
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
