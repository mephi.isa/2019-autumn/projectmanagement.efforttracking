package ru.mephi.projectmanagement.efforttracking.app.extension

import java.net.*

fun Throwable?.isNetworkException(): Boolean {
    return when (this) {
        is ConnectException,
        is SocketException,
        is SocketTimeoutException,
        is UnknownHostException,
        is ProtocolException -> true
        else -> false
    }
}
