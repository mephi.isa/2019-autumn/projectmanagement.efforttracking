package ru.mephi.projectmanagement.efforttracking.domain.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(

    @Json(name = "id")
    val id: Int,

    @Json(name = "fio")
    val fio: String,

    @Json(name = "role")
    val role: String,

    @Json(name = "managerId")
    val managerId: Int

)
