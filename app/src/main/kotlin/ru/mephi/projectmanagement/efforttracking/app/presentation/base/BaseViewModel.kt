package ru.mephi.projectmanagement.efforttracking.app.presentation.base

import androidx.lifecycle.ViewModel
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.CommandsLiveData
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ViewCommand
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    val commands = CommandsLiveData<ViewCommand>()

    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    // region Subscriptions

    fun <T> Single<T>.safeSubscribe(
        onSuccess: (T) -> Unit,
        onError: ((exception: Throwable, message: String) -> Unit)? = null
    ): Disposable {
        return subscribe(
            { onSuccess(it) },
            { throwable ->
                // FIXME Перенести текстовку в strings.xml.
                onError?.invoke(throwable, throwable.message ?: "Что-то пошло не так")
            }
        ).apply { compositeDisposable.add(this) }
    }
    // endregion

}
