package ru.mephi.projectmanagement.efforttracking.app.presentation

import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import javax.inject.Inject

class AppViewModel @Inject constructor() : BaseViewModel()
