package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state

enum class ErrorScreenState {
    ERROR,
    CONTENT
}
