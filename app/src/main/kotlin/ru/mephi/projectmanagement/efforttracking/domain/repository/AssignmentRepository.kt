package ru.mephi.projectmanagement.efforttracking.domain.repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment

interface AssignmentRepository {

    fun getAssignments(): Single<List<Assignment>>

    fun getAssignment(id: Int): Single<Assignment>

    fun setAssignment(assignment: Assignment): Completable

}
