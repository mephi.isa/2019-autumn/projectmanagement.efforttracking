package ru.mephi.projectmanagement.efforttracking.domain.extension

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat

private val timeFormat = DateTimeFormat.forPattern("HH:mm")
private val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
private const val MINUTES_HOUR = 60f

fun DateTime.toFormattedTime(): String = timeFormat.print(this)
fun DateTime.toFormattedDate(): String = dateFormat.print(this)

fun DateTime.toVerbalDate(): String {
    return when {
        isToday() -> "Сегодня"
        isYesterday() -> "Вчера"
        isDayBeforeYesterday() -> "Позавчера"
        else -> toFormattedDate()
    }
}

fun DateTime.isToday() = this.toLocalDate() == LocalDate()

fun DateTime.isYesterday() = this.toLocalDate() == LocalDate().minusDays(1)

fun DateTime.isDayBeforeYesterday() = this.toLocalDate() == LocalDate().minusDays(2)

fun String.toTime(): DateTime = timeFormat.parseDateTime(this)

fun String.toDate(): DateTime = dateFormat.parseDateTime(this)

fun Float.floatToTime(): DateTime {
    val hours = this.toInt()
    val minutes = (MINUTES_HOUR * (this - hours)).toInt()
    return LocalTime(hours, minutes).toDateTime(DateTime.now())
}

fun String.timeToFloat(): Float {
    return try {
        val dateTime = this.toTime()

        dateTime.hourOfDay + dateTime.minuteOfHour / MINUTES_HOUR
    } catch (e: IllegalArgumentException) {
        -1f
    }
}
