package ru.mephi.projectmanagement.efforttracking.app.extension

import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

@MainThread
inline fun <reified VM : ViewModel> FragmentActivity.viewModels(
    crossinline viewModelProducer: () -> VM
): Lazy<VM> {
    return lazy(LazyThreadSafetyMode.NONE) { createViewModel { viewModelProducer() } }
}

@MainThread
inline fun <reified VM : ViewModel> Fragment.viewModels(
    crossinline viewModelProducer: () -> VM
): Lazy<VM> {
    return lazy(LazyThreadSafetyMode.NONE) { createViewModel { viewModelProducer() } }
}

@MainThread
inline fun <reified VM : ViewModel> Fragment.activityViewModels(
    crossinline viewModelProducer: () -> VM
): Lazy<VM> {
    return lazy(LazyThreadSafetyMode.NONE) { requireActivity().createViewModel { viewModelProducer() } }
}

@MainThread
inline fun <reified VM : ViewModel> Fragment.createViewModel(
    crossinline viewModelProducer: () -> VM
): VM {
    val factory = object : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <VM : ViewModel> create(modelClass: Class<VM>) = viewModelProducer() as VM
    }
    val viewModelProvider = ViewModelProviders.of(this, factory)
    return viewModelProvider[VM::class.java]
}

@MainThread
inline fun <reified VM : ViewModel> FragmentActivity.createViewModel(
    crossinline viewModelProducer: () -> VM
): VM {
    val viewModelProvider = ViewModelProviders.of(
        this,
        object : ViewModelProvider.Factory {

            @Suppress("UNCHECKED_CAST")
            override fun <VM : ViewModel> create(modelClass: Class<VM>) = viewModelProducer() as VM
        }
    )
    return viewModelProvider[VM::class.java]
}
