package ru.mephi.projectmanagement.efforttracking.app.presentation.base.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.view_error.view.*
import ru.mephi.projectmanagement.efforttracking.R

class ErrorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var errorTitle = ""
        set(value) {
            field = value
            view_error_text_view_title.text = field
        }

    private var retryButton = ""
        set(value) {
            field = value
            view_error_button_retry.isVisible = value.isNotBlank()
            view_error_button_retry.text = field
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_error, this, true)

        lateinit var attrsArray: TypedArray

        try {
            attrsArray = context.obtainStyledAttributes(attrs, R.styleable.ErrorView)
            with(attrsArray) {
                errorTitle = getString(R.styleable.ErrorView_errorTitle) ?: ""
            }

        } finally {
            attrsArray.recycle()
        }
    }

    fun setOnRetryClickListener(listener: () -> Unit) {
        view_error_button_retry.setOnClickListener { listener() }
    }

    fun showError(
        isNetworkException: Boolean,
        @StringRes
        internetErrorTitleId: Int = R.string.error_no_internet,
        @StringRes
        dataErrorTitleId: Int = R.string.error_data_unavailable
    ) {

        val title = if (isNetworkException) internetErrorTitleId else dataErrorTitleId
        errorTitle = context.getString(title)

        val retryButtonText = R.string.try_again
        retryButton = context.getString(retryButtonText)
    }

    fun showStub(
        @StringRes
        titleId: Int = R.string.stub_title_empty_list,
        @StringRes
        retryButtonTextId: Int? = null
    ) {
        errorTitle = context.getString(titleId)
        retryButton = retryButtonTextId?.let { context.getString(it) } ?: ""
    }
}
