package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state

enum class UploadingScreenState {
    LOADING,
    CONTENT
}
