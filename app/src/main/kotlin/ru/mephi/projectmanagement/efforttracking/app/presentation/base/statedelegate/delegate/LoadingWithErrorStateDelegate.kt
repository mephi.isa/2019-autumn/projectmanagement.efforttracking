package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate

import android.view.View
import com.redmadrobot.lib.sd.base.State
import com.redmadrobot.lib.sd.base.StateChangeStrategy
import com.redmadrobot.lib.sd.base.StateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.changestrategy.ProgressStrategy
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.LoadingWithErrorScreenState
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.LoadingWithErrorScreenState.*

class LoadingWithErrorStateDelegate(
    contentViews: List<View>,
    loadingViews: List<View>,
    @Suppress("MaxLineLength")
    loadingStateChangeStrategy: StateChangeStrategy<LoadingWithErrorScreenState> = ProgressStrategy(loadingViews.last()),
    errorViews: List<View>
) : StateDelegate<LoadingWithErrorScreenState>(
    State(LOADING, loadingViews, loadingStateChangeStrategy),
    State(CONTENT, contentViews),
    State(ERROR, errorViews)
) {

    constructor(
        contentView: View,
        loadingView: View,
        loadingStateChangeStrategy: StateChangeStrategy<LoadingWithErrorScreenState> = ProgressStrategy(loadingView),
        errorView: View
    ) : this(
        listOf(contentView),
        listOf(loadingView),
        loadingStateChangeStrategy,
        listOf(errorView)
    )

    fun showContent() {
        currentState = CONTENT
    }

    fun showLoading() {
        currentState = LOADING
    }

    fun showError() {
        currentState = ERROR
    }
}
