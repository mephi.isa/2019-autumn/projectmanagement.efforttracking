package ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate

import android.view.View
import com.redmadrobot.lib.sd.base.State
import com.redmadrobot.lib.sd.base.StateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.changestrategy.NoChangeStrategy
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.changestrategy.ProgressStrategy
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.state.UploadingScreenState

class UploadingStateDelegate(
    progressView: View
) : StateDelegate<UploadingScreenState>(
    State(
        UploadingScreenState.LOADING,
        listOf(progressView),
        ProgressStrategy(progressView)
    ),
    State(
        UploadingScreenState.CONTENT,
        emptyList(),
        NoChangeStrategy()
    )
) {
    fun showContent() {
        currentState = UploadingScreenState.CONTENT
    }

    fun showLoading() {
        currentState = UploadingScreenState.LOADING
    }
}
