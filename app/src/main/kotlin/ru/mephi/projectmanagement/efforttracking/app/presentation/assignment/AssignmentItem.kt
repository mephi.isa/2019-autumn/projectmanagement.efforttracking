package ru.mephi.projectmanagement.efforttracking.app.presentation.assignment

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_assignment.*
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.domain.extension.toFormattedDate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment

data class AssignmentItem(
    private val assignment: Assignment,
    private val onClick: (Assignment) -> Unit
) : Item() {

    override fun getId() = assignment.id.toLong()

    override fun getLayout(): Int = R.layout.item_assignment

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder) {
            item_assignment_text_view_title.text = assignment.title

            with(item_assignment_text_view_dates) {
                text = context.getString(
                    R.string.date_interval,
                    assignment.startDate.toFormattedDate(),
                    assignment.endDate.toFormattedDate()
                )
            }

            item_assignment_container.setOnClickListener { onClick(assignment) }
        }
    }
}
