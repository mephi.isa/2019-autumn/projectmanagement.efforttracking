package ru.mephi.projectmanagement.efforttracking.domain.model

import com.squareup.moshi.JsonClass
import org.joda.time.DateTime

@JsonClass(generateAdapter = true)
data class Task(
    val name: String,
    val project: Project,
    val startDate: DateTime,
    val endDate: DateTime,
    val description: String?
)
