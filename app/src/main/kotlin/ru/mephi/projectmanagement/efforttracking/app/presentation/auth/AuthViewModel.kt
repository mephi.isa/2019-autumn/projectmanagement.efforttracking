package ru.mephi.projectmanagement.efforttracking.app.presentation.auth

import androidx.lifecycle.MutableLiveData
import ru.mephi.projectmanagement.efforttracking.app.extension.onNext
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.SchedulersProvider
import ru.mephi.projectmanagement.efforttracking.app.infrastructure.scheduleIoToUi
import ru.mephi.projectmanagement.efforttracking.app.presentation.auth.AuthViewState.Content
import ru.mephi.projectmanagement.efforttracking.app.presentation.auth.AuthViewState.Loading
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseViewModel
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.Next
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.ShowSnackbarError
import ru.mephi.projectmanagement.efforttracking.domain.repository.UserRepository
import timber.log.Timber
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val schedulers: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val TAG = "AuthViewModel"
    }

    val state = MutableLiveData<AuthViewState>()

    private var contentState: Content = Content()
        set(value) {
            field = value
            state.onNext(field)
        }

    fun onLoginChanged(login: String) {
        if (login == contentState.login) return

        contentState = contentState.copy(
            login = login,
            isEnterButtonEnabled = areParamsValid(login = login)
        )
    }

    fun onPasswordChanged(password: String) {
        if (password == contentState.password) return

        contentState = contentState.copy(
            password = password,
            isEnterButtonEnabled = areParamsValid(password = password)
        )
    }

    fun onEnterButtonClick() {
        userRepository
            .authorize(contentState.login, contentState.password)
            .scheduleIoToUi(schedulers)
            .doOnSubscribe { state.onNext(Loading) }
            .safeSubscribe(
                onSuccess = {
                    state.onNext(contentState)
                    commands.onNext(Next())
                },
                onError = { throwable, message ->
                    Timber.tag(TAG).e(throwable)
                    state.onNext(contentState)
                    commands.onNext(ShowSnackbarError(message))
                }
            )
    }

    private fun areParamsValid(
        login: String = contentState.login,
        password: String = contentState.password
    ): Boolean {
        return login.isNotBlank() && password.isNotBlank()
    }
}
