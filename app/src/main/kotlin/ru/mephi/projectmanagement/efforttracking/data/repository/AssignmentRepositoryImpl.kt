package ru.mephi.projectmanagement.efforttracking.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.mephi.projectmanagement.efforttracking.data.network.ServerAPI
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.Assignment
import ru.mephi.projectmanagement.efforttracking.domain.repository.AssignmentRepository
import ru.mephi.projectmanagement.efforttracking.domain.repository.UserRepository
import javax.inject.Inject

class AssignmentRepositoryImpl @Inject constructor(
    private val serverAPI: ServerAPI,
    private val userRepository: UserRepository
) : AssignmentRepository {

    private var assignments = mutableListOf<Assignment>()

    override fun getAssignments(): Single<List<Assignment>> {
        return Single
            .fromCallable { userRepository.getUser() }
            .flatMap { serverAPI.getTasks(it.id) }
            .doOnSuccess { assignments = it.toMutableList() }
    }

    override fun getAssignment(id: Int): Single<Assignment> {
        return Single.just(assignments)
            .flattenAsObservable { it }
            .filter { it.id == id }
            .firstOrError()
    }

    override fun setAssignment(assignment: Assignment): Completable {
        return Completable.fromCallable {
            val index = assignments.indexOfFirst { it.id == assignment.id }
            assignments.set(index, assignment)
        }
    }

}
