package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.fragment_edit_transaction.*
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.App
import ru.mephi.projectmanagement.efforttracking.app.extension.hideKeyboard
import ru.mephi.projectmanagement.efforttracking.app.extension.observe
import ru.mephi.projectmanagement.efforttracking.app.extension.setOnTextChangeListener
import ru.mephi.projectmanagement.efforttracking.app.extension.viewModels
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.BaseActivity
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.command.*
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.statedelegate.delegate.UploadingStateDelegate
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.EditTransactionViewState.Content
import ru.mephi.projectmanagement.efforttracking.app.presentation.transaction.EditTransactionViewState.Loading
import ru.mephi.projectmanagement.efforttracking.domain.extension.toFormattedDate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkRate
import ru.mephi.projectmanagement.efforttracking.domain.model.assignment.WorkType
import javax.inject.Inject

class EditTransactionFragment : Fragment(R.layout.fragment_edit_transaction) {

    companion object {
        private const val TRANSACTION_DATE_PICKER_TAG = "TransactionDatePicker"
    }

    val args: EditTransactionFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: EditTransactionViewModel.Factory

    private val viewModel by viewModels { viewModelFactory.create(args.assignmentId, args.transactionId) }

    private lateinit var screenState: UploadingStateDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as App).appComponent.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(viewModel.state, ::handleState)
        observe(viewModel.commands, ::handleCommand)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        screenState = UploadingStateDelegate(fragment_edit_transaction_container_progress)

        val titleId = if (args.transactionId == -1) {
            R.string.fragment_edit_transaction_toolbar_title_create
        } else {
            R.string.fragment_edit_transaction_toolbar_title_edit
        }

        with(fragment_edit_transaction_toolbar) {
            title = getString(titleId)
            setNavigationOnClickListener {
                requireView().findNavController().navigateUp()
            }
        }

        fragment_edit_transaction_edit_text_time.setOnTextChangeListener { viewModel.onTimeChanged(it) }

        with(fragment_edit_transaction_edit_text_date) {
            setOnClickListener { viewModel.onDateClick() }
            onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) requireActivity().hideKeyboard()
            }
        }

        fragment_edit_transaction_edit_text_comment.setOnTextChangeListener { viewModel.onCommentChanged(it) }

        with(fragment_edit_transaction_spinner_work_rate) {
            val items = WorkRate.values().map { it.value }
            adapter = android.widget.ArrayAdapter(context, android.R.layout.simple_list_item_1, items)

            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.onWorkRateChanged(WorkRate.values()[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    /* ignored */
                }
            }
        }

        with(fragment_edit_transaction_spinner_work_type) {
            val items = WorkType.values().map { it.value }
            adapter = android.widget.ArrayAdapter(context, android.R.layout.simple_list_item_1, items)

            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.onWorkTypeChanged(WorkType.values()[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    /* ignored */
                }
            }
        }

        fragment_edit_transaction_button_save.setOnClickListener {
            requireActivity().hideKeyboard()
            viewModel.onSaveClick()
        }
    }

    private fun handleState(viewState: EditTransactionViewState) {
        when (viewState) {
            Loading -> showLoading()
            is Content -> handleContentState(viewState)
        }
    }

    private fun showLoading() {
        screenState.showLoading()
    }

    private fun handleContentState(viewState: Content) {

        with(fragment_edit_transaction_edit_text_time) {
            if (text.toString() != viewState.time) setText(viewState.time)
        }

        with(fragment_edit_transaction_edit_text_date) {
            val date = viewState.date?.toFormattedDate() ?: ""
            if (text.toString() != date) setText(date)
        }

        fragment_edit_transaction_spinner_work_rate.setSelection(viewState.workRate.ordinal)
        fragment_edit_transaction_spinner_work_type.setSelection(viewState.workType.ordinal)

        with(fragment_edit_transaction_edit_text_comment) {
            if (text.toString() != viewState.comment) setText(viewState.comment)
        }

        fragment_edit_transaction_button_save.isEnabled = viewState.saveButtonEnabled
        screenState.showContent()
    }

    private fun handleCommand(command: ViewCommand) {
        when (command) {
            is ShowSnackbarMessage -> (requireActivity() as BaseActivity).showMessage(messageText = command.message)
            is ShowSnackbarError -> (requireActivity() as BaseActivity).showError(messageText = command.message)
            is Back -> requireView().findNavController().navigateUp()
            is ShowDatePicker -> {
                showDatePicker(
                    command.startDate,
                    command.endDate,
                    command.currentDate ?: DateTime.now().takeIf { it.isBefore(command.endDate) } ?: command.endDate
                )
            }
        }
    }

    private fun showDatePicker(startDate: DateTime, endDate: DateTime, currentDate: DateTime) {

        val calendarConstraintsBuilder = CalendarConstraints
            .Builder()
            .setStart(startDate.millis)
            .setEnd(endDate.millis)
            .setOpenAt(currentDate.millis)
            .setValidator(TransactionDateValidator(startDate, endDate))

        MaterialDatePicker
            .Builder
            .datePicker()
            .setSelection(currentDate.millis)
            .setCalendarConstraints(calendarConstraintsBuilder.build())
            .build()
            .apply {
                addOnPositiveButtonClickListener { viewModel.onDateChanged(DateTime(it)) }
                show(this@EditTransactionFragment.requireFragmentManager(), TRANSACTION_DATE_PICKER_TAG)
            }
    }

}

@Parcelize
class TransactionDateValidator(
    private val startDate: DateTime,
    private val endDate: DateTime
) : CalendarConstraints.DateValidator {

    override fun isValid(date: Long): Boolean {
        val dateTime = DateTime(date)

        return dateTime.isAfter(startDate) && dateTime.isBefore(endDate)
    }
}
