package ru.mephi.projectmanagement.efforttracking.domain.repository

import io.reactivex.Single
import ru.mephi.projectmanagement.efforttracking.domain.model.User

interface UserRepository {

    fun authorize(login: String, password: String): Single<User>

    fun getUser(): User?

    fun logout()

}
