package ru.mephi.projectmanagement.efforttracking.app.presentation.transaction

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_transaction_header.*
import org.joda.time.DateTime
import ru.mephi.projectmanagement.efforttracking.R
import ru.mephi.projectmanagement.efforttracking.app.presentation.base.recycler.sticky_header.StickyHeaderItem
import ru.mephi.projectmanagement.efforttracking.domain.extension.toVerbalDate

data class TransactionHeaderItem(private val date: DateTime) : Item(), StickyHeaderItem {

    override fun getId(): Long = date.millis

    override fun getLayout(): Int = R.layout.item_transaction_header

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        bindToViewHolder(viewHolder)
    }

    override fun bindToViewHolder(viewHolder: GroupieViewHolder) {
        viewHolder.item_transaction_header_text_view_date.text = date.toVerbalDate()
    }

}
